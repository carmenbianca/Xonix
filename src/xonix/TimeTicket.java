package xonix;

class TimeTicket extends WorldObject
{
    private int seconds;

    public TimeTicket (java.awt.geom.Point2D.Float location, java.awt.Color color, int width, int height, int seconds)
    {
        super(location, color, width, height);
        setSeconds (seconds);
    }

    public boolean contains (final java.awt.geom.Point2D.Float that)
    {
        java.awt.geom.Point2D.Float thisLoc = this.getLocation ();
        return that.x >= thisLoc.x && that.x <= thisLoc.x + this.getWidth () && that.y >= thisLoc.y && that.y <= thisLoc.y + this.getHeight ();
    }

    public int getSeconds ()
    {
        return seconds;
    }

    public void setSeconds (int seconds)
    {
        this.seconds = seconds;
    }

    @Override
    public String toString ()
    {
        return "";
        //return "loc=" + loc.x + "," + loc.y + " color=[" + color.getRed () + "," + color.getGreen () + "," + color.getBlue () + "]" + " width=" + width + " height=" + height + " seconds=" + seconds;
    }
}
